﻿using System;
using System.Diagnostics;
using System.Linq;
using BookComrade.Data;
using Microsoft.AspNetCore.Mvc;
using BookComrade.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System.IO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace BookComrade.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly AppDbContext context;

        public HomeController(
            AppDbContext context,
            RoleManager<IdentityRole> roleManager,
            UserManager<IdentityUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.context = context;
        }

        public IActionResult Index()
        {
            if (User.IsInRole("admin"))
                return Redirect("/Admin");
            if (User.IsInRole("standardUser") || User.IsInRole("premiumUser") )
                return Redirect("/Library");
            return Redirect("/Home/Create");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateSubscription()
        {
            var subscription = Request.Form["Subscription"].ToString();
            var user = await userManager.GetUserAsync(User);
            var subscriptionn = new Subscription
            {
                UserId = user.Id
            };
            switch (subscription)
            {
                case "standard":
                    await userManager.AddToRoleAsync(user, "standardUser");
                    break;
                case "premium":
                    await userManager.AddToRoleAsync(user, "premiumUser");
                    subscriptionn.IsPremium = true;
                    break;
                
            }
            context.Subscriptions.Add(subscriptionn);
            context.SaveChanges();
            return Redirect("Payment");
        }
        
        public IActionResult Payment()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult PaymentCheck()
        {
            var pay = Request.Form["pay"];
            if (pay.Equals("4"))
                return Redirect("/Home/Book");
            return Redirect("/Home/Payment");
        }

        [Authorize(Roles = "admin, standardUser, premiumUser")]
        public IActionResult Book()
        {
            var books = context.Books.ToList();
            if (User.IsInRole("standardUser"))
                books = context.Books.Where(x => !x.IsPremium).ToList();
            return View(books);
        }

        [Authorize(Roles = "admin, standardUser, premiumUser")]
        public IActionResult Read()
        {
            var bookId = int.Parse(Request.Query["bookId"]);
            var book = context.Books.Find(bookId);
            if (User.IsInRole("standardUser") && book.IsPremium)
                return NotFound();
            FileInfo file = new FileInfo(book.Path);
            if (file.Exists)
            {
                var contentType = "application/epub";
                byte[] fileBytes = System.IO.File.ReadAllBytes(file.FullName);
                return File(fileBytes, contentType, file.Name);
            }
            return View();
        }
        
        [Authorize(Roles = "admin, standardUser, premiumUser")]
        public async Task<IActionResult> Search()
        {
            var books = from b in context.Books select b;
            var searchResult = Request.Query["search"];
            if (!string.IsNullOrEmpty(searchResult))
                books = context.Books.Where(book => book.Name.Contains(searchResult) || book.Author.Contains(searchResult));
            books = User.IsInRole("standardUser")? books.Where(x=>!x.IsPremium) : books;
            return View(await books.ToListAsync());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddReview(IFormCollection form)
        {
            var text = form["Text"].ToString();
            var bookId = int.Parse(form["BookId"]);
            var rating = int.Parse(form["Rate"]);
            var user = await userManager.GetUserAsync(User);

            Review rev = new Review()
            {
                Text = text,
                BookId = bookId,
                Rate = (RateEnum)rating,
                Username = user.UserName
            };

            context.Reviews.Add(rev);
            context.SaveChanges();

            return RedirectToAction("Book", "Library", new { bookId });
        }
    }
}