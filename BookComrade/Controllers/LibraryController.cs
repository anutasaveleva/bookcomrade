﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using BookComrade.Data;
using BookComrade.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VersOne.Epub;
using HtmlAgilityPack;

namespace BookComrade.Controllers
{
    [Authorize(Roles = "admin, standardUser, premiumUser")]
    public class LibraryController : Controller
    {

        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly AppDbContext context;

        public LibraryController(
            AppDbContext context,
            RoleManager<IdentityRole> roleManager,
            UserManager<IdentityUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.context = context;
        }

        public IActionResult Index()
        {
            return User.IsInRole("standardUser")? View(context.Books.Where(x=>!x.IsPremium)) : View(context.Books);
        }

        public IActionResult Book()
        {
            var bookId = int.Parse(Request.Query["bookId"]);
            var book = context.Books.Find(bookId);
            if (User.IsInRole("standardUser") && book.IsPremium)
                return NotFound();
            var reviews = context.Reviews.Where(x => x.BookId == bookId);

            return View(new BookReview(){Book = book, Review = new Review(), Reviews = reviews});
        }

        public IActionResult Category()
        {
            var category = Enum.Parse(typeof(CategoryEnum),Request.Query["category"]);
            var books = User.IsInRole("standardUser") ? context.Books.Where(x => !x.IsPremium) : context.Books;
            return View(books.Where(book=>book.Category.Equals(category)));
        }
        
        public IActionResult Navigation()
        {
            var bookId = int.Parse(Request.Query["bookId"]);
            var book = context.Books.Find(bookId);
            using (EpubBookRef bookRef = EpubReader.OpenBook(book.Path))
                return View(Tuple.Create(bookRef.GetNavigation(), bookId));
            
        }
        public IActionResult ReadSection()
        {
            var bookId = int.Parse(Request.Query["bookId"]);
            var chapterName = (Request.Query["chapterName"]);
            var book = context.Books.Find(bookId);
            using (EpubBookRef bookRef = EpubReader.OpenBook(book.Path))
            {
                var chapter = bookRef.GetNavigation().Where(n => n.Title.Equals(chapterName)).First();
                var text = PrintTextContentFile(chapter.HtmlContentFileRef.ReadContent());
                ViewBag.HtmlStr = text;
                return View();
            }
          
        }
        private string PrintTextContentFile(String textContentFile)
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(textContentFile);
            StringBuilder sb = new StringBuilder();
            foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes("//text()"))
                sb.AppendLine(node.InnerText.Trim());
            return sb.ToString();
}
    }
}