using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BookComrade.Data;
using BookComrade.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VersOne.Epub;

namespace BookComrade.Controllers
{
    public class AdminViewModel
    {
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<IdentityUser> Users { get; set; }
    }

    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly AppDbContext context;
        private readonly ApplicationDbContext usercontext;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly IHostingEnvironment appEnvironment;

        public AdminController(
            AppDbContext context, ApplicationDbContext usercontext,
            RoleManager<IdentityRole> roleManager,
            UserManager<IdentityUser> userManager,
            IHostingEnvironment appEnvironment)

        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.usercontext = usercontext;
            this.context = context;
            this.appEnvironment = appEnvironment;
        }
        // GET
        public IActionResult Index() => View();

        public ActionResult Admin()
        {
            var userSubscriptions = new List<Tuple<Subscription, IdentityUser>>();
            foreach (var user in userManager.Users)
            {
                var sub = context.Subscriptions.FirstOrDefault(x => x.UserId == user.Id);
                userSubscriptions.Add(new Tuple<Subscription, IdentityUser>(sub,user));
            }
            /*foreach (var subscription in context.Subscriptions)
            {
                var user = userManager.Users.FirstOrDefault(x => x.Id == subscription.UserId);
                userSubscriptions.Add(new Tuple<Subscription, IdentityUser>(subscription,user));
            }*/
           return View(userSubscriptions);
        }

        public ActionResult AddBook() => View();

        public ActionResult ConfigureBook(Book book) => View(book);

        [HttpPost]
        public async Task<ActionResult> ConfigureBook(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                string path = "books/" + uploadedFile.FileName;
                using (var fileStream = new FileStream(path, FileMode.Create))
                    await uploadedFile.CopyToAsync(fileStream);

                EpubBook epubBook = EpubReader.ReadBook(path);

                var book = new Book
                {
                    Name = epubBook.Title,
                    Author = epubBook.Author,
                    Annotation = null,
                    IsPremium = false,
                    Path = path,
                    Category = CategoryEnum.Other,
                    Image = epubBook.CoverImage ?? epubBook.Content
                                                           .Images
                                                           .FirstOrDefault(b => b.Key.Contains("cover"))
                                                           .Value?
                                                           .Content
                };
                context.Books.Add(book);
                context.SaveChanges();
                return View(book);
            }
            return RedirectToAction("AddBook");
        }

        [HttpPost]
        public async Task<ActionResult> SaveBookInfoToDb(Book book, IFormFile uploadedFile)
        {
            var dbBook = context.Books.Find(book.Id);
            if (uploadedFile != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await uploadedFile.CopyToAsync(memoryStream);              
                    dbBook.Image = memoryStream.ToArray();
                }
            }
            dbBook.Name = book.Name;
            dbBook.IsPremium = book.IsPremium;
            dbBook.Category = book.Category;
            dbBook.Annotation = book.Annotation;

            context.Books.Update(dbBook);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ViewResult Edit(int subId)
        {
            Subscription sub = context.Subscriptions
                .FirstOrDefault(g => g.SubscriptionID == subId);
            return View(sub);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Subscription subscription)
        {
            var u = userManager.Users.FirstOrDefault(x => x.Id == subscription.UserId);
            if (u != null)
            {
                userManager.RemoveFromRolesAsync
                    (u, new List<string>() { "standardUser", "premiumUser" }).Wait();

                switch (subscription.IsPremium)
                {
                    case false:
                        await userManager.AddToRoleAsync(u, "standardUser");
                        break;
                    case true:
                        await userManager.AddToRoleAsync(u, "premiumUser");
                        break;
                }
            }
            context.Subscriptions.Update(subscription);
            context.SaveChanges();
            return RedirectToAction("Admin");
        }

        [HttpPost]
        public async Task<ActionResult> Create(Subscription subscription)
        {
            var u = userManager.Users.FirstOrDefault(x => x.Id == subscription.UserId);
            if (u != null)
            {
                userManager.RemoveFromRolesAsync
                    (u, new List<string>() { "standardUser", "premiumUser" }).Wait();
                switch (subscription.IsPremium)
                {
                    case false:
                        await userManager.AddToRoleAsync(u, "standardUser");
                        break;
                    case true:
                        await userManager.AddToRoleAsync(u, "premiumUser");
                        break;
                }
            }
            context.Subscriptions.Update(subscription);
            context.SaveChanges();
            return RedirectToAction("Admin");
        }

        public ViewResult Create(string userId)
        {
            var s = new Subscription() {UserId = userId};
            context.Subscriptions.Add(s);
            context.SaveChanges();
            return View("Create", s);
        }


        public ActionResult Delete(int subId)
        {
            Subscription deletedSub = context.Subscriptions.FirstOrDefault(x => x.SubscriptionID == subId);
            if (deletedSub != null)
            {
                var u = userManager.Users.FirstOrDefault(x => x.Id == deletedSub.UserId);
                if (u != null)
                    userManager.RemoveFromRolesAsync
                        (u, new List<string>() { "standardUser", "premiumUser" });
                context.Subscriptions.Remove(deletedSub);
                context.SaveChanges();
            }
            return RedirectToAction("Admin");
        }
    }
}