using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace BookComrade.Models
{
    public class ApplicationUser: IdentityUser
    {
        public virtual ICollection<Book> CustomTable{ get; set; }
    }
}