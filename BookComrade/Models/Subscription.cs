using System;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity;

namespace BookComrade.Models
{
    public class Subscription
    {
        public string UserId { get; set; }
        public int SubscriptionID { get; set; }
        public bool IsPremium { get; set; } = false;
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now.AddYears(1);
    }
}