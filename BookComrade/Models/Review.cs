using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace BookComrade.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public  RateEnum Rate { get; set; }
        public DateTime WhenPosted { get; set; } = DateTime.Now;
        
        public int BookId { get; set; }
        public string Username { get; set; }
    }

    public class BookReview
    {
        public Book Book { get; set; }
        public IEnumerable<Review> Reviews { get; set; }
        public Review Review { get; set; }
    }
}