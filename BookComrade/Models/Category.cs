﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BookComrade.Models
{
    public enum CategoryEnum
    {
        [Description("Классика")]
        Classic = 1,
        [Description("Поэзия")]
        Poetry = 2,
        [Description("Проза")]
        Prose = 3,
        [Description("Здоровье")]
        Health = 4,
        [Description("Саморазвитие")]
        Self_improvement = 5,
        [Description("Прочее")]
        Other = 6,
    }
    
   public static class CategoryEnumExtension
    {
        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .FirstOrDefault() as DescriptionAttribute;

                        if (descriptionAttribute != null)
                        {
                            return descriptionAttribute.Description;
                        }
                    }
                }
            }
            return null; // could also return string.Empty
        }
    }
}
