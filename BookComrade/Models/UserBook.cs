using Microsoft.AspNetCore.Identity;

namespace BookComrade.Models
{
    public class UserBook
    {
        public string UserId { get; set; }
        public IdentityUser User { get; set; }
        
        public int BookId { get; set; }
        public Book Book { get; set; }
    }
}