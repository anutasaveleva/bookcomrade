﻿using System;
using System.Collections.Generic;

namespace BookComrade.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Annotation { get; set; }
        public string Author { get; set; }
        public bool IsPremium { get; set; }
        public string Path { get; set; }
        public CategoryEnum Category { get; set; }
        public byte[] Image { get; set; }
    }
}
