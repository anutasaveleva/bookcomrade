using System.ComponentModel;

namespace BookComrade.Models
{
    public enum RateEnum
    {
        [Description("Не рекомендую")]
        BadImpression = 1,
        [Description("Неопределенное")]
        DontKnow = 2,
        [Description("На один раз")]
        OneRead = 3,
        [Description("Познавательно")]
        Interesting = 4,
        [Description("Великолепно")]
        Perfect = 5,
    }
}